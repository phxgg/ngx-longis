{
    "model": "Courses",
    "settings": {
      "i18n": {
        "el": {
          "Knowledge Area": "Γνωστικό αντικείμενο",
          "This is the unique code of the course": "Αυτός είναι ο μοναδικός αναγνωριστικός κωδικός του μαθήματος",
          "This is the display code which will be shown in transcripts, student course registration and online. Note that it might be used in third-party services as well.": "Αυτός είναι ο κωδικός εμφάνισης του μαθήματος, ο οποίος εμφανίζεται στις αναλυτικές βαθμολογίες, τις δηλώσεις μαθήματων και στο διαδίκτυο. Λάβετε υπόψιν ότι μπορεί να χρησιμοποιείται και από υπηρεσίες τρίτων.",
          "This is the course title.": "Αυτός είναι ο τίτλος του μαθήματος.",
          "Set the subtitle of the course. This field is optional": "Ορίστε τον υπότιτλο του μαθήματος. Αυτό το πεδίο είναι προαιρετικό",
          "Select if this course is active or has been discontinued.": "Επιλέξτε εάν το μάθημα θα είναι ενεργό ή έχει καταργηθεί.",
          "Input the number of units the course offers if passed.": "Ορίστε τον αριθμό των διδακτικών μονάδων που προσφέρει το μάθημα εάν περαστεί.",
          "Input the number of ECTS the course offers if passed.": "Ορίστε τον αριθμό των ECTS που προσφέρει το μάθημα εάν περαστεί.",
          "Select if the course will be shared. If yes, other departments will also be able to add this course to one of their study programs. If not, it will only be visible in your department": "Επιλέξτε εάν το μάθημα θα είναι κοινό. Εάν ναι, άλλα τμήματα θα μπορούν να προσθέσουν το μάθημα αυτό στα προγράμματα σπουδών τους. Εάν όχι, θα είναι διαθέσιμο μόνο στο τμήμα σας.",
          "Select if the course belongs to your institute": "Επιλέξτε εάν το μάθημα αυτό ανήκει στο ίδρυμα σας.",
          "Select if this course is counted in registration rules.": "Επιλέξτε εάν το μάθημα μετράει στους κανόνες δήλωσης μαθημάτων.",
          "Select the department this course will belong to. It defaults to your current department.": "Επιλέξτε το τμήμα στο οποίο θα ανήκει το μάθημα. Η προεπιλογή είναι το ενεργό τμήμα σας.",
          "Select the scientific supervising instructor of the course.": "Ορίστε τον επιστημονικό επιβλέπον καθηγητή του μαθήματος.",
          "Select the structure of the course. It can not be edited after course creation. The course has to be substituted by another course, with the same details and a different structure.": "Επιλέξτε την δομή του μαθήματος. Το πεδίο αυτό δεν μπορεί να αλλάξει μετά την δημιουργία του μαθήματος. Θα πρέπει να αντικατασταθεί από άλλο μάθημα με τις ίδιες ιδιότητες και διαφορετική δομή.",
          "The sort index of this course": "Η σειρά εμφάνισης του μαθήματος",
          "Sort index": "Σειρά"
        }
      }
    },
    "components": [{
        "label": "Tabs",
        "type": "tabs",
        "key": "tabs1",
        "path": "tabs1",
        "input": false,
  
        "components": [{
            "label": "General Information",
  
            "components": [{
        "type": "fieldset",
        "legend": "Course info",
        "components": [
          {
            "columns": [
              {
                "components": [
                  {
                    "label": "Course display code",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false
                    },
                    "key": "studyProgramCourse.course.displayCode",
                    "type": "textfield",
                    "input": true,
                    "description": "This is the display code which will be shown in transcripts, student course registration and online. Note that it might be used in third-party services as well."
                  }
                ],
                "width": 6
              },
              {
                "components": [
                  {
                    "label": "Course Structure Type",
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "searchEnabled": false,
                    "dataSrc": "url",
                    "data": {
                      "values": [],
                      "url": "/CourseStructureTypes?$top=-1&$skip=0&$select=id,name"
                    },
                    "dataType": "string",
                    "validate": {
                      "required": false
                    },
                    "selectThreshold": 0.3,
                    "lazyLoad": false,
                    "template": "{{ item.name }}",
                    "valueProperty": "id",
                    "selectValues": "value",
                    "key": "studyProgramCourse.course.courseStructureType",
                    "type": "select",
                    "input": true,
                    "disabled": true
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "Course Name",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": true
                    },
                    "key": "studyProgramCourse.course.name",
                    "type": "textfield",
                    "input": true,
                    "description": "This is the course title."
                  }
                ],
                "width": 8
              },
              {
                "components": [
                  {
                    "label": "Sort index",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false,
                      "min": 0
                    },
                    "key": "sortIndex",
                    "type": "number",
                    "input": true,
                    "description": "The sort index of this course"
                  }
                ],
                "width": 2
              },
              {
                "components": [
                  {
                    "label": "ID",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false
                    },
                    "key": "studyProgramCourse.course.id",
                    "type": "textfield",
                    "input": true,
                    "disabled": true,
                    "description": "This is the unique code of the course",
                    "hidden": true
                  }
                ],
                "width": 0
              },
              {
                "components": [
                  {
                    "label": "Subtitle",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false
                    },
                    "key": "studyProgramCourse.course.subtitle",
                    "type": "textfield",
                    "input": true,
                    "description": "Set the subtitle of the course. This field is optional"
                  }
                ],
                "width": 6
              },
              {
                "components": [
                  {
                    "label": "Enabled",
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "searchEnabled": false,
                    "data": {
                      "values": [
                        {
                          "label": "No",
                          "value": "0"
                        },
                        {
                          "label": "Yes",
                          "value": "1"
                        }
                      ]
                    },
                    "validate": {
                      "required": true
                    },
                    "calculateValue": "value = value? 1 : 0",
                    "selectThreshold": 0.3,
                    "key": "studyProgramCourse.course.isEnabled",
                    "type": "select",
                    "input": true,
                    "defaultValue": "0",
                    "logic": [
                      {
                        "name": "disableLogic",
                        "trigger": {
                          "type": "javascript",
                          "javascript": "result = (data.replacedByCourse != null)"
                        },
                        "actions": [
                          {
                            "name": "disableAction",
                            "type": "property",
                            "property": {
                              "label": "Disabled",
                              "value": "disabled",
                              "type": "boolean"
                            },
                            "state": true
                          }
                        ]
                    }],
                    "description": "Select if this course is active or has been discontinued."
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "Type",
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "data": {
                        "url": "CourseTypes?$orderby=name&$top=-1",
                        "headers": []
                    },
                    "template": "{{item.name}}",
                    "selectValues": "value",
                    "key": "courseType",
                    "valueProperty": "id",
                    "searchEnabled": true,
                    "validate": {
                        "required": true
                    },
                    "type": "select",
                    "input": true,
                    "lazyLoad": false
                }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "ECTS",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false,
                      "min": 0
                    },
                    "key": "ects",
                    "type": "number",
                    "input": true,
                    "description": "Input the number of ECTS the course offers if passed."
                  }
                ],
                "width": 3
              },
              {
                "components": [
                    {
                        "label": "Coefficient",
                        "mask": false,
                        "spellcheck": true,
                        "tableView": false,
                        "delimiter": false,
                        "requireDecimal": false,
                        "inputFormat": "plain",
                        "validate": {
                            "unique": false,
                            "multiple": false,
                            "required": true
                        },
                        "key": "coefficient",
                        "type": "number",
                        "input": true,
                        "hideOnChildrenHidden": false
                    }
                ],
                "width": 3
            },
              {
                "components": [
                  {
                    "label": "Grade Scale",
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "searchEnabled": true,
                    "dataSrc": "url",
                    "data": {
                      "values": [],
                      "url": "/GradeScales?$top=-1&$skip=0&$select=id,name"
                    },
                    "validate": {
                      "required": false
                    },
                    "selectThreshold": 0.3,
                    "lazyLoad": false,
                    "template": "{{ item.name }}",
                    "selectValues": "value",
                    "key": "studyProgramCourse.course.gradeScale",
                    "dataType": "object",
                    "idPath": "id",
                    "type": "select",
                    "input": true,
                    "disabled" : false
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "Course URL",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false
                    },
                    "key": "studyProgramCourse.course.courseUrl",
                    "type": "url",
                    "input": true
                  }
                ],
                "width": 6
              },
              {
                "components": [
                  {
                    "label": "Instructor",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "defaultValue": "",
                    "labelPosition": "top",
                    "data": {
                      "values": [],
                      "url": "/Instructors?$top={{limit}}&$skip={{skip}}&$select=id,familyName,givenName&$filter=indexof(familyName, '{{encodeURIComponent(search||'')}}') ge 0",
                      "headers": []
                    },
                    "lazyLoad": true,
                    "searchField": "search",
                    "searchEnabled": true,
                    "selectThreshold": 0.3,
                    "limit": 200,
                    "validate": {
                      "required": false
                    },
                    "dataType": "object",
                    "idPath": "id",
                    "template": "{{ item.familyName }} {{ item.givenName }}",
                    "key": "studyProgramCourse.course.instructor",
                    "type": "select",
                    "selectValues": "value",
                    "input": true,
                    "description": "Select the scientific supervising instructor of the course."
                  }
                ],
                "width": 6
              },
              {
                "components": [
                  {
                    "label": "Knowledge Area",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "defaultValue": "",
                    "labelPosition": "top",
                    "customConditional": "show = !!data.studyProgramCourse.course.department;",
                    "data": {
                      "values": [],
                      "url": "/CourseAreas?$top=-1&$skip=0&$select=id,name&$filter=department eq '{{data.studyProgramCourse.course.department}}'",
                      "headers": []
                    },
                    "validate": {
                      "required": false
                    },
                    "valueProperty": "id",
                    "lazyLoad": false,
                    "template": "{{ item.name }}",
                    "selectThreshold": 0.3,
                    "key": "studyProgramCourse.course.courseArea",
                    "type": "select",
                    "selectValues": "value",
                    "input": true
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "Course Category",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "labelPosition": "top",
                    "data": {
                      "url": "/CourseCategories?$top=-1&$skip=0&$select=id,name&$orderby=name",
                      "headers": []
                    },
                    "validate": {
                      "required": false
                    },
                    "lazyLoad": false,
                    "template": "{{ item.name }}",
                    "idPath": "id",
                    "selectThreshold": 0.3,
                    "key": "studyProgramCourse.course.courseCategory",
                    "type": "select",
                    "selectValues": "value",
                    "dataType": "object",
                    "input": true
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "CalculatedCoursePart",
                    "customConditional": "show = !!data.studyProgramCourse.course.parentCourse",
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "searchEnabled": false,
                    "data": {
                      "values": [
                        {
                          "label": "No",
                          "value": "0"
                        },
                        {
                          "label": "Yes",
                          "value": "1"
                        }
                      ]
                    },
                    "validate": {
                      "required": true
                    },
                    "selectThreshold": 0.3,
                    "key": "studyProgramCourse.course.calculatedCoursePart",
                    "selectValues": "value",
                    "type": "select",
                    "input": true,
                    "defaultValue": "1",
                    "calculateValue": "value = value? 1 : 0"
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "CoursePartPercent",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "dataType": "string",
                    "validate": {
                      "required": true,
                      "integer": true,
                      "min":0,
                      "max": 100
                    },
                    "key": "studyProgramCourse.course.coursePartPercent",
                    "type": "number",
                    "hidden": true,
                    "input": true
                  }
                ],
                "width": 3
              },
              {
                "components": [
                  {
                    "label": "Notes",
                    "labelPosition": "top",
                    "widget": {
                      "type": "input"
                    },
                    "validate": {
                      "required": false
                    },
                    "editor": "quill",
                    "key": "studyProgramCourse.course.notes",
                    "type": "textarea",
                    "input": true
                  }
                ],
                "width": 12
              }
            ],
            "customClass": "text-dark",
            "hideLabel": true,
            "type": "columns",
            "input": false
                }]
              }
            ]
          },
          {
            "legend": "Other languages",
            "key": "fieldSet",
            "type": "fieldset",
            "input": false,
            "label": "Other languages",
  
            "components": [{
              "label": "",
              "hideLabel": true,
              "key": "studyProgramCourse.course.locales",
              "type": "datagrid",
              "disableAddingRemovingRows": true,
              "input": true,
              "customClass": "fix-panel-for-locales",
              "components": [{
                "collapsible": false,
                "hideLabel": true,
                "key": "panel",
                "type": "panel",
                "title": "",
                "hideTitle": true,
                "input": false,
                "tableView": false,
                "components": [{
                    "label": "Language",
                    "tableView": true,
                    "key": "inLanguage",
                    "disabled": "true",
                    "type": "textfield",
                    "input": true
                  },
                  {
                    "label": "Name",
                    "tableView": true,
                    "key": "name",
                    "type": "textfield",
                    "input": true
                  }
                ]
              }]
            }]
          }
        ]
      },
      {
        "type": "button",
        "label": "Submit",
        "key": "submit",
        "disableOnInvalid": true,
        "input": true
      }
    ]
  }
  